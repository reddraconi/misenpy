# misenpy

A python recipe manager

## Features

- Management for multiple levels of collections
  - Single recipes
  - Dishes, made of multiple recipes
  - Menus, made of multiple dishes
- Import of recipes from web-based locations and local files (planned)

## Python Requirements

This module uses the following modules behind-the-scenes:

- loguru
  - A handy logging framework
- pynutbutter
  - Another one of our modules that connects to the USDA Nutritional Database
    to calculate info on your recipe.
    > **Note**: If networking is disabled via the configuration file, the module
    > will be required, but not used and it will not make outbound calls to
    > the USDA Nutritional Database.
- xdg
  - A handy module to make sure we're placing files into XDG-compliant
    locations on your computer.

## Usage

By default, you can run `python -m misenpy` to get a good set of defaults.
To get more detail about the defaults, you can run
`python -m misenpy --confused` to print out the defaults. After that, you can
change the defaults to meet your needs.

> **Note**: If you have a system-wide proxy configured (either via environment
> variables or another method), any outbound communications *should* use them
> by default. You can optionally specify HTTP and HTTPS proxies within the
> configuration file to override system-provided ones.

You can enable additional logging by providing one or more `-v` flags when
running `misenpy`: `python -m misenpy -vvv ...`.

To override the default configuration file location, you can pass the
`-c <config file>` option to `misenpy`.

To import recipes into your personal database, you can run
`python -m misenpy -i <filename> <filename> ... <filename>`. We currently
support the following formats:

- None

To export recipes, meals, or menus from your database, run
`python -m misenpy -e <ID or title> -ef <format>`. We support the following
formats:

- None

To have `misenpy` attempt to scrape a URL for a recipe, run
`python -m misenpy -s <URL>`. The `-s` option cannot be used with other options!

## Logging

If the program needs to emit any messages, they're automatically logged into a
log file that will rotate every 100MB. If you don't know where your
 configurations are located, you can run misenpy thusly:

```python
python -m misenpy --confused
```

Example Output (will change based on OS):

```text
misenpy Configuration:
  Config File:      /home/<username>/.conf/misenpy/misenpy.ini
  Cache Directory:  /home/<username>/.conf/misenpy/cache/
  Data Directory:   /home/<username>/.conf/misenpy/data/
  Logs Directory:   /home/<username>/.conf/misenpy/logs/
  Current Log File: /home/<username>/.conf/misenpy/logs/misenpy_<YYYY-MM-DD>.log
```

## Exit Codes

| Code | Reason |
|---|---|
| 0 | No errors. |
| 1 | There was an issue accessing a file or directory. |
| 2 | The requested file had a bad format or unknown filetype. |

Any exit codes that are greater than `0` should be preceeded by a log or
statement printed to the console.

## License

MIT

## Contributing

The code in this repository is formatted with the included `.style.yapf` file
using the Google YAPF module, available at pypi.org.

Any PRs will be run through `bandit` and `mypy` to ensure they're fairly safe.
If any issues are found, they'll be append to the PR and returned for work.
