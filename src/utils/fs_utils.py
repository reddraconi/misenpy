from os import makedirs
from pathlib import Path



def file_exists(input_file: Path) -> bool:
  from utils.config import conf
  from utils.logging import logg

  if conf.verbosity > 0:
    logg.debug(f"Attempting to verify '{input_file}' exists.")
  if not Path(input_file).is_file:
    logg.error(f"'{input_file}' does not exist!")
    return False

  if conf.verbosity > 0:
    logg.debug(f"Verified '{input_file}' exists!")
  return True

def create_path(file_path: Path) -> None:
  from utils.logging import logg
  try:
    makedirs(file_path, exist_ok=True)
  except Exception as e:
    logg.critical(f"Unable to create file path: '{file_path}'!: {e}")
    exit(1)