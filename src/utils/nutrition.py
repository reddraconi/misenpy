# Daily Values based on FDA Code of Federal Regulations, Title 21
# https://www.ecfr.gov/current/title-21/chapter-I/subchapter-B/part-101#p-101.9(c)(8)(iv)
# https://www.ecfr.gov/current/title-21/chapter-I/subchapter-B/part-101#p-101.9(c)(9)
daily_values = {
  "fat": {
    "measure": "g",
    "standard": 78,
    "infants": 30,
    "children": 39,
    "pregnant": 78,
  },
  "sat_fat": {
    "measure": "g",
    "standard": 20,
    "infants": None,
    "children": 10,
    "pregnant": 20,
  },
  "cholesterol": {
    "measure": "mg",
    "standard": 300,
    "infants": None,
    "children": 300,
    "pregnant": 300,
  },
  "carbohydrates": {
    "measure": "g",
    "standard": 275,
    "infants": 95,
    "children": 150,
    "pregnant": 275,
  },
  "sodium": {
    "measure": "mg",
    "standard": 2300,
    "infants": None,
    "children": 1500,
    "pregnant": 2300,
  },
  "dietary_fiber": {
    "measure": "g",
    "standard": 28,
    "infants": None,
    "children": 14,
    "pregnant": 28,
  },
  "protein": {
    "measure": "g",
    "standard": 50,
    "infants": None,
    "children": 25,
    "pregnant": 50,
  },
  "sugars_added": {
    "measure": "g",
    "standard": 50,
    "infants": None,
    "children": 25,
    "pregnant": 50,
  },
  "vit_d": {
    "measure": "mcg",
    "standard": 20,
    "infants": 10,
    "children": 15,
    "pregnant": 15
  }
}