from configparser import ConfigParser
from pathlib import Path
from sys import exit

from xdg import xdg_cache_home as cachehome
from xdg import xdg_config_home as confighome
from xdg import xdg_data_home as datahome

from utils.logging import logg


class config:

  def __init__(
    self,
    conffile=None,
  ):
    self.cachedir = Path(cachehome(), "misenpy")
    self.configdir = Path(confighome(), "misenpy")
    self.config = ConfigParser()
    self.datahome = Path(datahome(), "misenpy")
    self.networking = False
    self.conffile = conffile
    self.log_location = Path(self.datahome, "logs")
    self.log_file = Path(self.log_location, "misenpy.log")
    self.log_level = "logg.DEBUG"
    self.http_proxy = None
    self.https_proxy = None
    self.verbosity = 0

    if self.conffile is not None:
      self.process_config()

    self.validate_config()

  def process_config(self):
    if self.conffile is None:
      self.conffile = Path(self.configdir, "misenpy.ini")
      logg.warning(
        f"No configuration file provided. We'll use {self.conffile}."
      )

    self.config.read_file(open(self.conffile))  # type: ignore

    if self.config.get('DEFAULT', 'log_level', fallback=None) is not None:
      self.default_loglevel = self.config.get('DEFAULT', 'logging.level')
    else:
      self.config.set('DEFAULT', 'logging.level', "INFO")

    if self.config.get('DEFAULT', 'log_location', fallback=None) is not None:
      self.log_location = self.config.get('DEFAULT', 'log_location')

    if self.config.get('DEFAULT', 'enable_netowrking',
                       fallback=False) is not False:
      self.networking = self.config.getboolean('DEFAULT', 'enable_networking')

    # if self.config.get('PROXY', 'http_proxy', fallback=None) is not None:
    #   self.http_proxy = self.config.get('PROXY', 'http_proxy')
    # elif (environ['HTTP_PROXY'] is not None):
    #   self.http_proxy = environ['HTTP_PROXY']

    # if self.config.get('PROXY', 'https_proxy', fallback=None) is not None:
    #   self.https_proxy = self.config.get('PROXY', 'https_proxy"')
    # elif (environ['HTTPS_PROXY'] is not None):
    #   self.http_proxy = environ['HTTPS_PROXY']

  def validate_config(self):
    logg.debug(f"Validating configuration file.")

    dirs = [self.cachedir, self.configdir, self.datahome, self.log_location]
    for d in dirs:
      try:
        if not Path(self.cachedir).exists:
          logg.debug(f"'{d}' not found. Creating.")
          Path(d).mkdir
      except Exception as e:
        logg.critical(f"Unable to locate directory '{d}': {e}")
        exit(1)

    # Check log level is correct.

    log_levels = [
      "CRITICAL",
      "WARNING",
      "NOTICE",
      "INFO",
      "DEBUG"
    ]
    if self.conffile is not None:
      if self.log_level not in log_levels:
        logg.warning(
          f"'log_level' is incorrect in {self.conffile}. It must be one of: "
          f"'CRITICAL', 'WARNING', 'NOTICE', 'INFO', or 'DEBUG'. "
          f"Defaulting to INFO'."
        )
        self.config.set('DEFAULT', 'log_level', "INFO")
    else:
      self.config.set('DEFAULT', 'log_level', "INFO")

    # Check proxy format is correct.

    # if self.networking:
    #   if self.http_proxy is not None:
    #     try:
    #       req = Request('http://google.com')
    #       req.set_proxy(self.http_proxy, 'http')
    #       response = urlopen(req)
    #       print(response)
    #     except IOError as e:
    #       logg.critical(
    #         f"HTTP proxy connection error! Please check it is correct -- "
    #         f"Error: {e}"
    #       )
    #       exit(1)
    #   if self.https_proxy is not None:
    #     try:
    #       req = Request('https://google.com')
    #       req.set_proxy(self.https_proxy, 'https')
    #       response = urlopen(req)
    #       print(response)
    #     except IOError as e:
    #       logg.critical(
    #         f"HTTPS proxy connection error! Please check it is correct -- "
    #         f"Error: {e}"
    #       )
    #       exit(1)

  def set_verbosity(self, verbosity: int):
    if verbosity > 3:
      self.verbosity = 3
    elif verbosity < 0:
      self.verbosity = 0
    else:
      self.verbosity = verbosity

conf = config()