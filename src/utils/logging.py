import logging
from logging.handlers import RotatingFileHandler
from pathlib import Path
from typing import Union

from utils.fs_utils import create_path

logging.basicConfig(
  format="%(asctime)s [%(levelname)s]: %(message)s",
  datefmt="%Y%m%d %H:%M:%S%z",
  encoding="utf-8",
  filemode="a",
  level=logging.INFO
)
# To make sure logs are as quick as possible, we turn off things we don't need
# to collect to avoid extra system calls.
logging.logThreads = False
logging.logProcesses = False
logging.logMultiprocessing = False

class logg:
  """Creates a global logger for the application"""
  def __init__(self) -> None:
    self.file_path = None
    self.rotation_size_b = 100 * (1024 ** 1024) #100MB
    self.log_retention = 10
    self.logger = logging.getLogger('misenpy')

  def set_log_path(self, log_file_path: Path) -> None:
    """Configures log retentnion and writing logs to disk"""
    create_path(log_file_path)

    self.file_path = log_file_path

    handler = RotatingFileHandler(
      Path(log_file_path, "misenpy.log"),
      maxBytes=self.rotation_size_b,
      backupCount=self.log_retention
    )
    self.logger.addHandler(handler)

  def set_log_level(self, log_level: Union[str,int]) -> None:
    """Sets the logger to the provided level"""
    if isinstance(log_level, str):
      self.logger.setLevel(map_to_log_level(log_level))
    elif isinstance(log_level, int):
      self.logger.setLevel(log_level)

  def critical(self, log_message: str) -> None:
    self.logger.critical(log_message)

  def error(self, log_message: str) -> None:
    self.logger.error(log_message)

  def warning(self, log_message: str) -> None:
    self.logger.warning(log_message)

  def info(self, log_message: str) -> None:
    self.logger.info(log_message)

  def debug(self, log_message: str) -> None:
    self.logger.debug(log_message)

  def get_logger(self) -> logging.Logger:
    return self.logger

def map_to_log_level(log_level: str) -> int:
  """ Maps a string log level to the correct integer value

  Defaults to 'INFO' level if an unknown level is encountered.

  params:
  - log_level (str): Some string
  returns:
  - (int): The log level, per
    https://docs.python.org/3/howto/logging.html#logging-levels

  """
  level = log_level.upper()

  if level == "CRITICAL":
    return 50
  elif level == "ERROR":
    return 40
  elif level == "WARNING":
    return 30
  elif level == "DEBUG":
    return 10
  else:
    return 20 #INFO

logg = logg()