from datetime import datetime, timezone
from enum import Enum
from typing import Optional
from typing_extensions import Self
import re


class measure(Enum):
  PINCH = {
    'id': 'pinch',
    'short': 'pinch',
    'long': 'pinch',
    'short_p': 'pinches',
    'long_p': 'pinches'
  }
  DASH = {
    'id': 'dash',
    'short': 'dash',
    'long': 'dash',
    'short_p': 'dashes',
    'long_p': 'dashes'
  }
  TEASPOON = {
    'id': 't',
    'short': 'tsp',
    'short_p': 'tsps',
    'long': 'teaspoon',
    'long_p': 'teaspoons'
  }
  TABLESPOON = {
    'id': 'T',
    'short': 'tbl',
    'short_p': 'tbls',
    'long': 'tablespoon',
    'long_p': 'tablespoons'
  }
  CUP = {
    'id': 'C',
    'short': 'cup',
    'short_p': 'cups',
    'long': 'cup',
    'long_p': 'cups'
  }
  FLUIDOZ = {
    'id': 'floz.',
    'short': 'fl oz.',
    'short_p': 'fl ozs.',
    'long': 'fluid ounce',
    'long_p': 'fluid ounces'
  }
  OZ = {
    'id': 'oz',
    'short': 'oz',
    'short_p': 'ozs',
    'long': 'ounce',
    'long_p': 'ounces'
  }
  PINT = {
    'id': 'pt',
    'short': 'pt',
    'short_p': 'pts',
    'long': 'pint',
    'long_p': 'pints'
  }
  QUART = {
    'id': 'qt',
    'short': 'quart',
    'short_p': 'quarts',
    'long': 'quart',
    'long_p': 'quarts'
  }
  GALLON = {
    'id': 'gal',
    'short': 'gal',
    'short_p': 'gals',
    'long': 'gallon',
    'long_p': 'gallons'
  }
  POUND = {
    'id': 'lb',
    'short': 'lb',
    'short_p': 'lbs',
    'long': 'pound',
    'long_p': 'pounds'
  }
  GRAM = {'id': 'g', 'short': 'g', 'long': 'gram', 'long_p': 'grams'}
  LITER = {'id': 'l', 'short': 'l', 'long': 'liter', 'long_p': 'liters'}


class ingredient:
  """ An individual component of a recipe with optional replacement(s)

  Args:
    name: The name of the ingredient (yellow onion, all purpose flour, etc.)
    amount: The amount, as float
    measure: The type of measure (measure.CUP, measure.TSP, etc.)
    preparation: String annotating the preparation of the ingredient (diced,
      minced, boiled, etc.). None by default.
    option: Is the ingredient optional? False by default.
    alternate_ingredients: A list of ingredient objects that can be used to
      replace the exact amount provided in this ingredient.
  """

  def __init__(
    self,
    name: str,
    amount: float,
    measure: measure,
    preparation: Optional[str] = None,
    optional: bool = False,
    alternate_ingredients: list[Self] = []
  ):
    self.name = name
    self.amount = amount
    self.measure = measure
    self.preparation = preparation
    self.optional = optional
    self.alternate_ingredients = alternate_ingredients

  def add_alternate_ingredient(self, replacement: Self) -> None:
    """
    replacement: an ingredient object.
    """
    self.alternate_ingredients.append(replacement)


class author:
  """ A recipe, dish, or menu author
  
  Args:
    name: The name of the author
    mailing_address: The mailing address of the author (optional)
    email_address: The email address of the author (optional)
    site: The URL of the author's website (optional)
  """

  def __init__(
    self,
    name: str,
    mailing_address: Optional[str] = None,
    email_address: Optional[str] = None,
    site: Optional[str] = None
  ):
    self.name = name
    self.mailing_address = mailing_address
    self.email_address = email_address
    self.site = site


class recipe:
  """ A collection of ingredients, their preparation, and some metadata
  
  Args:
    title: The title of the recipe.
    pub_date: The date the recipe was published.
    author: A list of author(s) of the recipe.
    ingredients: A list of ingredient objects used to make the recipe.
    directions: A list of steps used to prepare the recipe.
    equipment: An optional list of equipment used to prepare the dish.
    servings: A freeform string of servings yielded by the dish.
    notes: Any additional notes provided by the creator. None by default.
    source: An optional source (url, book, etc.). None by default.
  """

  def __init__(
    self,
    title: str,
    pub_date: Optional[str] = None,
    author: list[author] = [],
    ingredients: list[ingredient] = [],
    directions: list[str] = [],
    equipment: list[str] = [],
    servings: int = 1,
    notes: list[str] = [],
    source: list[str] = [],
  ):
    self.title = title
    if (pub_date is None):
      self.pub_date = datetime.now(timezone.utc).isoformat()
    else:
      # TODO: Enforce ISO8601 formatting
      self.pub_date = pub_date
    self.author = author
    self.ingredients = ingredients
    self.directions = directions
    self.equipment = equipment
    self.servings = servings
    self.notes = notes
    self.source = source
    self.nutrition = self.gen_nutrition()

  def add_servings(self, servings: str) -> None:
    self.servings = servings

  def add_equipment(self, equipment: list[str]) -> None:
    for e in equipment:
      self.equipment.append(e)

  def add_ingredients(self, ingredients: list[ingredient]) -> None:
    for i in ingredients:
      self.ingredients.append(i)

  def gen_nutrition(self):
    if self.ingredients:
      #TODO: Create an aggregate of nutritional information based on the
      #      ingredients, divided by the number of servings. If no servings are
      #      available, we provide the entire result for the whole recipe.
      pass
    else:
      return None


class dish:
  """ A dish is a collection of recipes, used as a meal.

  Args:
    title: The name of the dish
    pub_date: The publish date of the dish
    recipes: A list of recipes that make up a dish
    preparation: An ordered list of steps to make the dish
    notes: Optional notes about the dish.
  """

  def __init__(
    self,
    title: str,
    pub_date: str,
    recipes: list[recipe],
    preparation: list[str],
    notes: list[str] = [],
  ):
    self.title = title
    if pub_date is None:
      self.pub_date = datetime.now(timezone.utc).isoformat()
    else:
      #TODO: Enforce ISO8601 formatting
      self.pub_date = pub_date
    self.recipes = recipes
    self.preparation = preparation
    self.notes = notes


class menu:

  def __init__(self, title: str, pub_date: str, dishes: list[dish]):
    self.title = title
    if pub_date is None:
      self.pub_date = datetime.now(timezone.utc).isoformat()
    else:
      #TODO: Enforce ISO8601 formatting
      self.pub_date = pub_date
    self.dishes = dishes
    self.categories = {}

  def add_category(self, category: str, dishes: Optional[list[dish]]) -> None:
    category = category.title()
    category = re.sub(' ', '_', category)
    category = re.sub('[^a-zA-Z0-9_-]', '', category)
    self.categories[category] = []
    if dishes:
      self.add_dishes_to_category(category=category, dishes=dishes)

  def add_dishes_to_category(self, category: str, dishes: list[dish]) -> None:
    self.categories[category] = dishes
