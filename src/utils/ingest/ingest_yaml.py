from pathlib import Path

from yaml import YAMLError, safe_load

from utils.config import conf
from utils.fs_utils import file_exists
from utils.logging import logg


def ingest(input_file: Path):
  if not file_exists(input_file):
    return
  logg.info(f"Importing yaml file '{input_file}'")

  try:
    if conf.verbosity > 0:
      logg.debug(f"Attempting safe_load of yaml file '{input_file}'")
    with open(input_file, 'r') as file:
      doc = safe_load(file)
  except YAMLError as e:
    if hasattr(e, 'problem_mark'):
      logg.critical(
        f"There is a syntax error in the file '{input_file}' at "
        f"{e.problem_mark.line+1}:{e.problem_mark.column+1}"
      )
      exit(2)
    else:
      logg.critical(f"There is a problem with '{input_file}': {e}")
      exit(1)
  except Exception as e:
    logg.critical(f"We encountered an error when importing a file: {e}!")
    exit(1)

  # Do stuff with 'doc'.
  if conf.verbosity > 1:
    logg.debug(f"YAML import complete.")