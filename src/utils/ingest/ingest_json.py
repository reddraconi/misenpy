from json import JSONDecodeError
from json import load as json_load
from pathlib import Path

from utils.config import conf
from utils.fs_utils import file_exists
from utils.logging import logg


def ingest(input_file: Path):
  if not file_exists(input_file):
    return
  logg.info(f"Importing JSON file '{input_file}'")

  try:
    if conf.verbosity > 0:
      logg.debug(f"Attempting to load JSON file '{input_file}'")
    with open(input_file, 'r') as file:
      doc = json_load(file)
  except JSONDecodeError as e:
    logg.error(
      f"Syntax error encountered when loading provided JSON file "
      f"'{input_file}': {e.msg}:L{e.lineno}:C{e.colno}"
    )
    exit(2)
  except Exception as e:
    logg.critical(f"We encountered an error when importing a file: {e}!")
    exit(1)

  # Do stuff with 'doc'

  if conf.verbosity > 1:
    logg.debug(f"JSON import complete.")
