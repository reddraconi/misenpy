#/usr/bin/env python

import optparse
from pathlib import Path

import utils.ingest.ingest_json as ingest_json
import utils.ingest.ingest_yaml as ingest_yaml
from utils.config import conf
from utils.logging import logg

# Globals ######################################################################

good_yaml_suffixes = ['.yml', 'yaml']
good_json_suffixes = ['.json']
good_import_suffixes = good_yaml_suffixes + good_json_suffixes
guessed_extension = False

# Command-line option processing ###############################################

oparser = optparse.OptionParser()
oparser.add_option(
  '--config',
  '-c',
  action="store",
  help="Configuration file",
  dest="config_file"
)
oparser.add_option(
  '--confused', help="I need help finding my files!", action="store_true"
)
oparser.add_option('-v', action="count", dest="verbosity")
oparser.add_option(
  '--quiet', '-q', action="store_const", const=0, dest="verbosity", default=0
)
oparser.add_option(
  '--import',
  '-i',
  dest="import_list",
  action="append",
  default=[],
  help="List of recipe files to import"
)
oparser.add_option(
  '--export',
  '-e',
  dest="export_list",
  action="append",
  default=[],
  help="List of recipes to export"
)
oparser.add_option(
  '--format',
  '-f',
  type="choice",
  choices=['pdf', 'txt', 'mkd', 'rml', 'yml', 'json'],
  dest="format"
)
oparser.add_option(
  '--scrape',
  '-s',
  action="store",
  help="Scrape a site. Provide a URL.",
  dest="scrape_url"
)

if __name__ == "__main__":
  (options, args) = oparser.parse_args()

  ## Check commandline inputs ##################################################

  if options.export_list and not options.format:
    oparser.error("-e/--export requires an export format using -f/--format.")

  if options.scrape_url and (options.import_list or options.export_list):
    oparser.error("-s/--scrape cannot be used with other options.")

  conf.set_verbosity(options.verbosity)

  if options.config_file is not None:
    conf = conf.parse_config(options.config_file)
    conf = conf.validate_config()

  if options.confused:
    print(
      f"""misenpy Configuration:
        Config File:     {conf.conffile}
        Cache Directory: {conf.cachedir}
        Data Directory:  {conf.datahome}
        Logs Directory:  {conf.log_location}
        Verbosity:       {conf.verbosity}
      """
    )
    exit(0)

  if not (options.import_list or options.export_list or options.scrape_url):
    oparser.error(
      "No actions were provided. Must provide '-s/--scrape', '-e/--export', or "
      "'-i/--import'. You can run '-h' to see all available options."
    )

  ## Setup debug logging #######################################################

  if (conf.verbosity) > 0:
    logg.set_log_level(10)

  ## Enter main application loop ###############################################

  # if options.export_list:
  #   if options.verbosity != 0:
  #     print("Exporting '$_thing'.")
  #   pass

  #TODO: Toss this later.
  if options.export_list or options.scrape_url:
    raise NotImplementedError

  if options.import_list and not options.format:
    logg.warning(
      "Format not provided for import request. We'll make our best guess."
    )
    guessed_extension = True

    for i in options.import_list:
      if Path(i).suffix not in good_import_suffixes:
        logg.error(f"'{i}' has an unknown or unsupported extension. Skipping.")
        continue

      if Path(i).suffix in good_yaml_suffixes:
        if conf.verbosity > 0 and guessed_extension:
          logg.debug(f"Guessed 'yaml' for {i}.")
        ingest_yaml.ingest(i)

      if Path(i).suffix in good_json_suffixes:
        if conf.verbosity > 0 and guessed_extension:
          logg.debug(f"Guessed 'json' for {i}.")
        ingest_json.ingest(i)

  logg.info(f"misenpy exiting.")
  exit(0)